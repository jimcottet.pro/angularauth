import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, tap } from 'rxjs';
import { User } from './entities';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * Un BehaviorSubject est un Observable dans lequel on peut manuellement pousser
   * des données, il va nous permettre de récupérer notre user, ou absence de user
   * dans les components où on en a besoin
   */
  private _user:BehaviorSubject<User|null> = new BehaviorSubject<User|null>(null);

  constructor(private http:HttpClient) { }
  /**
   * Méthode permettant de récupérer le user en utilisant le cookie de session, elle
   * sera utilisée au lancement de l'application pour obtenir les informations de la
   * personne actuellement connectée pour les mettre dans le authService
   */
  getUser() {
    return this.http.get<User>('http://localhost:8080/api/user/account').pipe(
    
      tap(data => this._user.next(data))
    );
  }
  /**
   * La méthode login va envoyer les identifiants du User vers le serveur et, dans le
   * cas où les identifiants sont correct, va pousser le user connectée dans le BehaviorSubject
   * @param email identifiant du User
   * @param password mot de passe du User
   * @returns Renvoie le User sous forme d'Observable
   */
  login(email:string, password:string) {
    return this.http.get<User>('http://localhost:8080/api/user/account', {
      headers: {
        'Authorization': 'Basic '+btoa(email+':'+password),
      }
    }).pipe(
      tap(data => this._user.next(data)) //On pousse le user connectée dans le subject
    );

  }
  /**
   * Méthode pour l'inscription, qui, si l'inscription réussie, nous connecte automatiquement
   * @param user Le User à faire persister
   * @returns Le User qui a persisté
   */
  register(user:User) {
    return this.http.post<User>('http://localhost:8080/api/user', user).pipe(
      tap(data => this._user.next(data)) //On pousse le user connectée dans le subject
    );
  }
  /**
   * La méthode logout qui va stoper la session côté serveur et poussé un null dans le
   * subject afin que le front sache qu'on est pu connecté
   */
  logout() {
    return this.http.get<void>('http://localhost:8080/logout').pipe(
      tap(() => this._user.next(null))
    );
  }
  /**
   * Pas absolument obligatoire, mais pour faire en sorte qu'on ne puisse pousser
   * des nouvelles données dans le subject seulement depuis le service, on fait en sorte
   * que depuis l'extérieur on ait accès qu'à un simple Observable
   */
  get user() {
    return this._user.asObservable();
  }
}
