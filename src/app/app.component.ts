import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-auth';
  constructor(private auth:AuthService) {}

  ngOnInit(): void {
      this.auth.getUser().subscribe({
        error: () => console.log('not connected')
      });
  }

  logout() {
    this.auth.logout().subscribe();
  }
}
