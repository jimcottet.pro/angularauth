# AngularAuth

Projet angular avec de l'authentification, connectée à une API en Spring Boot

## Nécessaire pour l'authentification
Déjà, pour ce qui est du type d'authentification utilisé dans ce projet, il s'agit de HTTP Basic Auth, qui va utiliser des cookies de sessions et maintenir une session côté API (ce qui fait que techniquement, l'API n'est pas une API "REST", mais plutôt du RCP, vu que le ST de REST veut dire "STateless" et que les sessions sont un state).

Une autre manière assez utilisée pour gérer l'authentification entre un back et un front est avec des JWT. Mais ces derniers sont assez fastidieux à configurer avec Spring Boot.

Côté front, on aura donc besoin :
### Une méthode de connexion
```typescript
login(email:string, password:string) {
    return this.http.get<User>('http://localhost:8080/api/user/account', {
        headers: {
            'Authorization': 'Basic '+btoa(email+':'+password),
        }
    });
}
```
Comme on utilise le HTTP Basic Auth, on doit envoyer les identifiants de connexion directement dans les headers de la requête avec l'email et le mot de passe concaténés et convertis en base64. (Techniquement, n'importe quelle route protégée côté Spring peut faire office de route de connexion, ici, on part sur une route qui nous renvoie aussi les informations du user connecté)

### Une méthode d'inscription
```typescript
register(user:User) {
    return this.http.post<User>('http://localhost:8080/api/user', user);
}
```
Qui va donc poster un nouveau user sur la base de données

### Envoyer et récupérer les cookies
Pour que le HTTP Basic auth avec cookie de session fonctionne avec angular, il faut que celui ci dise à ses requêtes d'envoyer et de récupérer les cookies, pour ça on peut rajouter un `withCredentials:true` sur chaque requête qu'on execute.

Exemple:
```typescript
register(user:User) {
    return this.http.post<User>('http://localhost:8080/api/user', user, {withCredentials:true});
}
```

Mais une manière un peu plus pratique sera de faire ce qu'on appel un interceptor qui est une classe spéciale en angular qui viendra, comme son nom l'indique, intercepter toutes les requêtes faites avec HttpClient et appliquera une modification dessus.

Dans [cet interceptor](src/app/credential.interceptor.ts), on fait fait en sorte de rajouter le withCredentials dans chacune des requêtes qui sera effectuée :

```typescript
intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({
      setHeaders: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      withCredentials: true
    });
    
    return next.handle(request);
  }
```

On rajoute également un header X-Requested-With pour éviter notamment qu'une requête non authentifiée nous affiche la boîte de dialogue par défaut de login, qui est moche, et qu'on préfèrera gérer entièrement côté front.


### Gérer l'état de connexion
Pour afficher l'interface comme il faut selon l'état de connexion et selon les informations du user connecté, s'il est présent, il faut une manière de stocker quelque part le user en question.

Pour ça, on peut partir sur le fait de stocker le user actuellement connecté directement dans le AuthService
```typescript
private _user:BehaviorSubject<User|null> = new BehaviorSubject<User|null>(null);

get user() {
    return this._user.asObservable();
}
```
On le stock sous forme de BehaviorSubject, un genre d'observable dans lequel on peut pousser des valeurs manuellement.

L'idée sera de faire en sorte que pour chaque requête qui modifie l'état de connexion, on pousse la nouvelle valeur, ou absence de valeur dans ce `_user`. Pour ce faire, on utilise l'opérateur tap() de rxjs (qui permet d'exécuter une fonction à chaque fois qu'un observable emet une valeur)
```typescript
requeteLogin().pipe(
    tap(data => this._user.next(data)) //On pousse le user connectée dans le subject
);
```

Pour utiliser ce user dans les templates, il faudra injecter le AuthService dans le component associé, puis subscribe sur le user et l'assigner à une propriété du component ([exemple complet](src/app/home/home.component.ts))

```typescript
this.auth.user.subscribe(user => this.user = user);
```